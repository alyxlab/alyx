use std::hash::Hash;

/// One-dimensional space point.
#[derive(Copy, Debug)]
pub struct Point1D {
    x: f64,
}

impl Point1D {
    /// One-dimensional origin point.
    const ORIGIN: Self = Self { x: 0.0 };
    /// Creates a new `Point1D`.
    ///
    /// # Panics
    ///
    /// Panics if `x` is `NaN`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use geometry::line::Point1D;
    /// let point = Point1D::new(1.0);
    /// ```
    pub fn new(x: f64) -> Self {
        assert!(!x.is_nan());
        assert!(!x.is_infinite());
        assert!(!x.is_subnormal());
        Point1D { x }
    }

    /// Returns the origin point.
    pub fn origin() -> Self {
        Self::ORIGIN
    }

    /// Returns the `x` coordinate of the point.
    pub fn x(&self) -> f64 {
        self.x
    }

    /// Returns the coordinates `(x)` of the point.
    pub fn coordinates(&self) -> (f64,) {
        (self.x,)
    }
}

// No-op clone implementation
impl Clone for Point1D {
    #[inline]
    fn clone(&self) -> Self {
        *self
    }
}

// Defaults to origin and returns always the same instance.
impl Default for Point1D {
    #[inline]
    fn default() -> Self {
        Self::ORIGIN
    }
}

// Implements `Eq` and `Hash` based on correct handling of NaN in constructor.
impl PartialEq for Point1D {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x
    }
}
impl Eq for Point1D {}
impl Hash for Point1D {
    #[inline]
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        state.write_u64(self.x.to_bits());
    }
}

// Implements `Ord` based on correct handling of NaN in constructor.
impl PartialOrd for Point1D {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.x.partial_cmp(&other.x)
    }
}
impl Ord for Point1D {
    #[inline]
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.partial_cmp(other) {
            Some(ord) => ord,
            None => unreachable!(),
        }
    }
}

#[cfg(test)]
mod tests {
    #![allow(clippy::float_cmp)]
    use super::*;
    use std::{cmp::Ordering, collections::hash_map::DefaultHasher, hash::Hasher};

    #[test]
    fn new() {
        let point = Point1D::new(1.0);
        assert_eq!(point.x, 1.0);
    }

    #[test]
    fn origin() {
        let point = Point1D::origin();
        assert_eq!(point.x, 0.0)
    }

    #[test]
    fn coordinates() {
        let point = Point1D::new(1.0);
        assert_eq!(point.coordinates(), (1.0,));
    }

    #[test]
    fn x() {
        let point = Point1D::new(1.0);
        assert_eq!(point.x(), 1.0);
    }

    #[test]
    #[allow(clippy::clone_on_copy)]
    fn clone() {
        let point1 = Point1D::new(1.0);
        let point2 = point1.clone();
        assert_eq!(point1.x, point2.x);
    }

    #[test]
    fn copy() {
        let point1 = Point1D::new(1.0);
        let point2 = point1;
        assert_eq!(point1.x, point2.x);
    }

    #[test]
    fn default() {
        let point = Point1D::default();
        assert_eq!(point.x, 0.0)
    }

    #[test]
    fn eq() {
        let point1 = Point1D::new(1.0);
        let point2 = Point1D::new(1.0);
        assert_eq!(point1, point2);
    }

    #[test]
    fn hash() {
        let point1 = Point1D::new(1.0);
        let point2 = Point1D::new(1.0);
        let mut hasher = DefaultHasher::new();
        point1.hash(&mut hasher);
        let hash1 = hasher.finish();
        let mut hasher = DefaultHasher::new();
        point2.hash(&mut hasher);
        let hash2 = hasher.finish();
        assert_eq!(hash1, hash2);
    }

    #[test]
    fn cmp() {
        let min = Point1D::new(0.0);
        let mid = Point1D::new(1.0);
        let max = Point1D::new(2.0);
        // asymmetry & total
        assert_eq!(min.cmp(&max), Ordering::Less);
        assert_eq!(mid.cmp(&mid), Ordering::Equal);
        assert_eq!(max.cmp(&min), Ordering::Greater);
        // transitivity
        assert_eq!(mid.cmp(&mid.clone()), Ordering::Equal);
        assert_eq!(min.cmp(&mid), Ordering::Less);
        assert_eq!(mid.cmp(&max), Ordering::Less);
        assert_eq!(min.cmp(&max), Ordering::Less);
        assert_eq!(mid.cmp(&min), Ordering::Greater);
        assert_eq!(max.cmp(&mid), Ordering::Greater);
        assert_eq!(max.cmp(&min), Ordering::Greater);
    }
}
