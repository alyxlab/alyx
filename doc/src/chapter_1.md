# Geometry

## Line (1D -- one-dimensional space)

Alyx provides following entities for 1D geometry:
- [Point1D](https://alyxlab.gitlab.io/alyx/doc/geometry/line/struct.Point1D.html)
- [Segment1D](https://alyxlab.gitlab.io/alyx/doc/geometry/line/struct.Segment1D.html)
- [Ray1D](https://alyxlab.gitlab.io/alyx/doc/geometry/line/struct.Ray1D.html)
