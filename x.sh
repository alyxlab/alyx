set -ex

cargo check --workspace --all-features --all-targets
cargo clippy --workspace --all-features --all-targets -- -D warnings
cargo fmt --all -- --check
cargo build --workspace --all-features --all-targets
cargo test --workspace --all-features --all-targets
cargo test --workspace --all-features --doc
cargo doc --workspace --all-features --no-deps
mdbook test doc
mdbook build doc
