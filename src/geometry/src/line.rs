//! One-dimensional geometry space.
mod point;
pub use point::Point1D;
mod ray;
pub use ray::Direction1D;
pub use ray::Ray1D;
mod segment;
pub use segment::Segment1D;
