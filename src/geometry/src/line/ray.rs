use super::Point1D;

/// One-dimensional Ray.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Ray1D {
    endpoint: Point1D,
    direction: Direction1D,
}

/// One-dimensional direction.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Direction1D {
    /// Towards positive infinity.
    Positive,
    /// Towards negative infinity.
    Negative,
}

impl Ray1D {
    /// Creates a new `Ray1D`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use geometry::line::{Direction1D, Point1D, Ray1D};
    /// let point = Point1D::origin();
    /// let direction = Direction1D::Positive;
    /// let ray = Ray1D::new(point, direction);
    /// ```
    pub fn new(endpoint: Point1D, direction: Direction1D) -> Self {
        Ray1D {
            endpoint,
            direction,
        }
    }
}
