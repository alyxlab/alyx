use super::Point1D;

/// One-dimensional segment.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Segment1D(Point1D, Point1D);

impl Segment1D {
    /// Creates a new `Segment1D` from two points.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use geometry::line::{Point1D, Segment1D};
    /// let a = Point1D::new(1.0);
    /// let b = Point1D::new(2.0);
    /// let segment = Segment1D::new(a, b);
    /// ```
    pub fn new(a: Point1D, b: Point1D) -> Self {
        assert_ne!(a, b);
        Segment1D(a, b)
    }

    /// Returns points of the segment.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use geometry::line::{Point1D, Segment1D};
    /// let a = Point1D::new(1.0);
    /// let b = Point1D::new(2.0);
    /// let segment = Segment1D::new(a, b);
    /// assert_eq!(segment.points(), (a, b));
    /// ```
    pub fn points(&self) -> (Point1D, Point1D) {
        (self.0, self.1)
    }

    /// Returns the length of the segment.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use geometry::line::{Point1D, Segment1D};
    /// let a = Point1D::new(0.0);
    /// let b = Point1D::new(1.0);
    /// let segment = Segment1D::new(a, b);
    /// let length = segment.length();
    /// assert_eq!(length, 1.0);
    /// ```
    pub fn length(&self) -> f64 {
        (self.0.x() - self.1.x()).powi(2).sqrt()
    }
}
