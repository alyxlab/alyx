# Alyx

## Documentation

### Book

[Alyx Book](https://alyxlab.gitlab.io/alyx/book/index.html)

### API Documentation

[Alyx API documentation](https://alyxlab.gitlab.io/alyx/doc/geometry/index.html)
